export class Usuario {


    constructor(usuario?: string, password?:string){
        this.usuario = usuario;
        this.password = password;
    }    


    id: number;
    tipo_documento: any;
    password: any;
    nombres: string;
    apellidos: string;
    email: string;
    documento: any;
    telefono: any;
    usuario: any;

    rol: number;
  }