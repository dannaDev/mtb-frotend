import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}



declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}

export const ROUTESADMIN: RouteInfo[] = [
  {
    path: "/dashboard",
    title: "Dashboard",
    icon: "ni-tv-2 text-primary",
    class: ""
  },
  { path: '/pagos', title: 'Pagos',  icon:'ni ni-money-coins', class: '' },
  { path: '/usuarios', title: 'Usuarios',  icon:'ni ni-circle-08', class: '' },
 ];
 export const ROUTESUSUARIO: RouteInfo[] = [
  { path: '/mis-pagos', title: 'Mis Pagos',  icon:'ni ni-money-coins', class: '' },

 ];
 export let ROUTES: RouteInfo[] = [];

 @Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  public menuItems: any[];
  public isCollapsed = true;
  rol: string;

  constructor(private router: Router, private usuario: AuthService) { }

  ngOnInit() {
    // this.menuItems = ROUTES.filter(menuItem => menuItem);
 

   let usuario: any = sessionStorage.getItem('usuario');
   let rol = JSON.parse(usuario)
   switch (rol.rol) {
 
     case 1:
     this.menuItems = ROUTESADMIN.filter(menuItems => menuItems);
     this.rol = "Admin";
     break;
 
     case 2:
       this.menuItems = ROUTESUSUARIO.filter(menuItem => menuItem);
       ROUTES = ROUTESUSUARIO;
       this.rol = 'Usuario';
       break;
     
     default:
       break;
   }
   this.router.events.subscribe((event) => {
    this.isCollapsed = true;
 });
  }



}
