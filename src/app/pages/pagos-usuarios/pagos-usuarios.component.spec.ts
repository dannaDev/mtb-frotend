import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Pagos-usuariosUsuariosComponent } from './pagos-usuariosUsuarios-usuarios-usuarios.component';

describe('Pagos-usuariosUsuariosComponent', () => {
  let component: Pagos-usuariosUsuariosComponent;
  let fixture: ComponentFixture<Pagos-usuariosUsuariosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Pagos-usuariosUsuariosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Pagos-usuariosUsuariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
