import { Component, OnInit } from '@angular/core';
import Chart from 'chart.js';
import { AppService } from 'src/app/services/app.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-pagos-usuarios',
  templateUrl: './pagos-usuarios.component.html',
  styleUrls: ['./pagos-usuarios.component.scss']
})
export class PagosUsuariosComponent implements OnInit {

  public pagos: Array<any>=[];

  constructor(private servicio: AppService) { 

  }


ngOnInit() {
    this.getPagos();
  }


public getPagos(){
  let usuario: any = sessionStorage.getItem('usuario');
  let id = JSON.parse(usuario)
  this.servicio.get(`api/pago/usuario/${id.id}`).subscribe(
    (result: any) => {
      this.pagos = result.data;
    },
     error => { console.log(error)}
  );
}


}
