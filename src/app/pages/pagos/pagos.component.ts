import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { AppService } from 'src/app/services/app.service';
import { animate, style, transition, trigger } from '@angular/animations';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-pagos',
  templateUrl: './pagos.component.html',
  styleUrls: ['./pagos.component.scss'],
  animations: [
    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({ transform: 'translateX(100%)', opacity: 0 }),
          animate('300ms', style({ transform: 'translateX(0)', opacity: 1 }))
        ]),
        transition(':leave', [
          style({ transform: 'translateX(0)', opacity: 1 }),
          animate('300ms', style({ transform: 'translateX(100%)', opacity: 0 }))
        ])
      ]
    )
  ],
})
export class PagosComponent implements OnInit {

  public pagos: Array<any>=[];
  public documento: any;

  constructor(private servicio: AppService, private spinner: NgxSpinnerService) { 

  }


ngOnInit() {
    this.getPagos();
  }


public getPagos(){
  this.servicio.get('api/pago').subscribe(
    (result: any) => {
      this.pagos = result.data;
    },
     error => { console.log(error)}
  );
}

public filterPagos(){
  this.servicio.get(`api/pagos/filter/${this.documento}`).subscribe(
    (result: any) => {
      this.pagos = result.data;
    },
     error => { console.log(error)}
  );
}

public desactivar(item: any) {

  Swal.fire({
    title: 'Advertencia',
    text: 'Está seguro de que quiere eliminar este pago?',
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Si, desactivar',
    confirmButtonClass: 'btn btn-info',
    cancelButtonText: 'No, Cancelar'
  }).then((result) => {
    if (result.value) {
      this.servicio.put('api/pago/desactivar/'.concat(item.id), null).subscribe(
        (data: any) => {
          this.getPagos()
          Swal.fire({
            type: 'success', text: 'El pago ' + String(item.id).toUpperCase() + ' Ha sido eliminado!',
            timer: 3000, showConfirmButton: false
          });        },error => {
          console.log(error)
        }
      );
         
       
    } else if (result.dismiss === Swal.DismissReason.cancel) {
    }
  }) 

}
}

