import { Component, OnInit, ViewChild, HostListener } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AppService } from "src/app/services/app.service";
import { Usuario } from "src/app/models/usuario.model";
import { AuthService } from "src/app/services/auth.service";
import { NgxSpinnerService } from "ngx-spinner";
import Swal from "sweetalert2";

@Component({
  selector: "app-login",
  templateUrl: "login.component.html",
  styleUrls: ["login.component.scss"],
})
export class LoginComponent implements OnInit {
  public datos: FormGroup;
  public Toast = Swal.mixin({
    toast: true,
    position: "top-end",
    showConfirmButton: false,
    timer: 3000,
  });

  constructor(
    private _formBuilder: FormBuilder,
    private servicio: AppService,
    private authService: AuthService  ) {}

  ngOnInit() {
    this.datos = this._formBuilder.group({
      usuario: ["", Validators.compose([Validators.required])],
      password: ["", Validators.compose([Validators.required])],
    });
  }

  public login() {
      this.servicio
        .login(
          new Usuario(
            this.datos.controls["usuario"].value,
            this.datos.controls["password"].value
          )
        )
        .subscribe(
          async (result: any) => {
            if(result.httpCode === 200){
              this.Toast.fire({
                type: "success",
                title: "Bienvenido",
              });
              this.authService.guardarUsuario(result);
            }

            if (result.httpCode === 401) {
              this.Toast.fire({
                type: "error",
                title: result.error,
              });
            }
           
          },
         
        );
    } 
    }
  

