import { animate, style, transition, trigger } from "@angular/animations";
import {
  Component, EventEmitter,
  Inject, OnInit,
  Output
} from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators
} from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { NgxSpinnerService } from "ngx-spinner";
import { AppService } from "src/app/services/app.service";
import Swal from "sweetalert2";

@Component({
  selector: "app-usuarios-form",
  templateUrl: "./usuarios-form.component.html",
  styleUrls: ["./usuarios-form.component.scss"],
  animations: [
    trigger("enterAnimation", [
      transition(":enter", [
        style({ transform: "translateX(100%)", opacity: 0 }),
        animate("300ms", style({ transform: "translateX(0)", opacity: 1 })),
      ]),
      transition(":leave", [
        style({ transform: "translateX(0)", opacity: 1 }),
        animate("300ms", style({ transform: "translateX(100%)", opacity: 0 })),
      ]),
    ]),
  ],
})
export class UsuariosFormComponent implements OnInit {
  @Output() volvergrupo = new EventEmitter<string>();
  public dataList: any;

  volver() {
    this.volvergrupo.emit();
  }
  public datos: FormGroup;
  submitted = false;
  public customPatterns = { "0": { pattern: new RegExp("[0]") } };
  private Toast = Swal.mixin({
    toast: true,
    position: "top-end",
    showConfirmButton: false,
    timer: 3000,
  });
  display: boolean = false;

  constructor(
    private _formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<UsuariosFormComponent>,
    private servicio: AppService,
    private spinner: NgxSpinnerService
  ) {
    this.datos = this._formBuilder.group({
      documento: [
        "",
        Validators.compose([Validators.required, Validators.maxLength(10)]),
      ],
      nombres: ["", Validators.required, Validators.pattern("[a-zA-Z ]*")],
      apellidos: ["", Validators.required, Validators.pattern("[a-zA-Z ]*")],
      email: ["", Validators.required, Validators.email],
      telefono: ["", Validators.required],
    });
  }

  get f() {
    return this.datos.controls;
  }

  ngOnInit() {
  }

  onSubmit() {
    this.submitted = true;

    if (!this.datos.invalid) {
      this.crear();
    }
  }

  public crear() {
    Swal.fire({
      title: "Advertencia",
      text: "Está seguro de que quiere agregar este usuario?",
      type: "warning",
      showCancelButton: true,
      confirmButtonText: "Si, agregar",
      confirmButtonClass: "btn btn-info",
      cancelButtonText: "No, Cancelar",
    }).then((result) => {
      if (result.value) {
        this.servicio
          .post("api/register", this.datos.value)
          .subscribe((data: any) => {
            Swal.fire({
              type: "success",
              text: data.mensaje,
              timer: 3000,
              showConfirmButton: false,
            });
            this.close(1);
          });
      }
    });
  }

  public close(tipo: number): void {
    this.dialogRef.close(tipo);
  }
}
