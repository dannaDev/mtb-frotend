import {
  Component,
  ElementRef,
  EventEmitter,
  Inject,
  Input,
  OnInit,
  Output,
  ViewChild,
  ViewEncapsulation,
} from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
  ReactiveFormsModule,
} from "@angular/forms";
import { animate, style, transition, trigger } from "@angular/animations";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { RxwebValidators } from "@rxweb/reactive-form-validators";
import Swal from "sweetalert2";
import { AppService } from "src/app/services/app.service";
import { NgxSpinnerService } from "ngx-spinner";
import { DatePipe } from "@angular/common";

@Component({
  selector: "app-pagos-form",
  templateUrl: "./pagos-form.component.html",
  styleUrls: ["./pagos-form.component.scss"],
  providers: [DatePipe],

  animations: [
    trigger("enterAnimation", [
      transition(":enter", [
        style({ transform: "translateX(100%)", opacity: 0 }),
        animate("300ms", style({ transform: "translateX(0)", opacity: 1 })),
      ]),
      transition(":leave", [
        style({ transform: "translateX(0)", opacity: 1 }),
        animate("300ms", style({ transform: "translateX(100%)", opacity: 0 })),
      ]),
    ]),
  ],
})
export class PagosFormComponent implements OnInit {
  @Output() volvergrupo = new EventEmitter<string>();
  public dataList: any;

  volver() {
    this.volvergrupo.emit();
  }
  public datos: FormGroup;
  public customPatterns = { "0": { pattern: new RegExp("[0]") } };
  private Toast = Swal.mixin({
    toast: true,
    position: "top-end",
    showConfirmButton: false,
    timer: 3000,
  });
  display: boolean = false;
  public tipo_pago = [
    {
      nombre: "EFECTIVO",
    },
    {
      nombre: "TARJETA DE CRÉDITO",
    },
    {
      nombre: "TRANSFERENCIA",
    },
  ];
  constructor(
    private _formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<PagosFormComponent>,
    private servicio: AppService,
    private spinner: NgxSpinnerService,
    private datePipe: DatePipe
  ) {
    const myDate = new Date();
    const fecha = this.datePipe.transform(myDate, "yyyy-MM-dd");
    this.datos = this._formBuilder.group({
      fecha: [
        fecha,
        Validators.compose([Validators.required, Validators.maxLength(10)]),
      ],
      tipopago: ["", Validators.required, Validators.pattern("[a-zA-Z ]*")],
      descripcion: ["", Validators.required, Validators.pattern("[a-zA-Z ]*")],
    });
  }

  ngOnInit() {
    console.log(this.data.data);
  }

  public crear() {
    const datos = this.datos.value;
    const informacion = {
      usuario: this.data.data.id,
      tipo_pago: datos.tipopago,
      fecha: datos.fecha,
      descripcion: datos.descripcion,
    };
    Swal.fire({
      title: "Advertencia",
      text: "Está seguro de que quiere agregar este pago?",
      type: "warning",
      showCancelButton: true,
      confirmButtonText: "Si, agregar",
      confirmButtonClass: "btn btn-info",
      cancelButtonText: "No, Cancelar",
    }).then((result) => {
      if (result.value) {
        this.servicio
          .post("api/pago/new", informacion)
          .subscribe((data: any) => {
            Swal.fire({
              type: "success",
              text: data.mensaje,
              timer: 3000,
              showConfirmButton: false,
            });
            this.close(1);
          });
      }
    });
  }
  public close(tipo: number): void {
    this.dialogRef.close(tipo);
  }
}
