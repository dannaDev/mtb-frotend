import { animate, style, transition, trigger } from '@angular/animations';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NgxSpinnerService } from 'ngx-spinner';
import { AppService } from 'src/app/services/app.service';
import Swal from 'sweetalert2';
import { PagosFormComponent } from './pagos-form/pagos-form.component';
import { UsuariosFormComponent } from './usuarios-form/usuarios-form.component';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.scss'],
  animations: [
    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({ transform: 'translateX(100%)', opacity: 0 }),
          animate('300ms', style({ transform: 'translateX(0)', opacity: 1 }))
        ]),
        transition(':leave', [
          style({ transform: 'translateX(0)', opacity: 1 }),
          animate('300ms', style({ transform: 'translateX(100%)', opacity: 0 }))
        ])
      ]
    )
  ],

})
export class UsuariosComponent implements OnInit {

  public usuarios: Array<any>=[];

  public EncuestasUsuarioID:  Array<any> = [];
  public EncuestasAll: Array<any> = [];
  constructor(  public dialog: MatDialog, private servicio: AppService, private spinner: NgxSpinnerService) { 

  }
  private Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
  });

ngOnInit() {
    
    this.getUsuarios()      

  }

public nuevo() {
  this.openDialog(0);
  
}


getUsuarios(){
  this.spinner.show();
  this.servicio.get('api/usuarios').subscribe(
    (result: any) => {this.spinner.hide();
      this.usuarios = result.data;
    },
     error => { console.log(error), this.spinner.hide() }
  );
}


public openDialog(tipoForm: number){
  const dialogRef = this.dialog.open(UsuariosFormComponent, {
    width: '700px',
    height: '523px',
    disableClose : true ,
  });
  dialogRef.afterClosed().subscribe(result => {
    if (result === 1) {
      this.getUsuarios();
    }
  });
}

public openDialogPagos(item:any){
  const dialogRef = this.dialog.open(PagosFormComponent, {
    width: '700px',
    height: '523px',
    disableClose : true ,
    data: { data: item }
  });
  dialogRef.afterClosed().subscribe(result => {
    if (result === 1) {
      this.getUsuarios();
    }
  });
}

}
