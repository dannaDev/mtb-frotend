import { Routes } from '@angular/router';

import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import { PagosComponent } from 'src/app/pages/pagos/pagos.component';
import { UsuariosComponent } from 'src/app/pages/usuarios/usuarios.component';

export const AdminLayoutRoutes: Routes = [
    { path: 'dashboard',      component: DashboardComponent },
    { path: 'pagos',   component: PagosComponent },
    { path: 'usuarios',         component: UsuariosComponent },
];
