import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ClipboardModule } from 'ngx-clipboard';

import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PagosComponent } from 'src/app/pages/pagos/pagos.component';
import { UsuariosComponent } from 'src/app/pages/usuarios/usuarios.component';
import { UsuariosFormComponent } from 'src/app/pages/usuarios/usuarios-form/usuarios-form.component';
import { MatDialogModule, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material/dialog';
// import { ToastrModule } from 'ngx-toastr';
import { ButtonModule } from 'primeng-lts/button';
import { NgxMaskModule } from 'ngx-mask';
import { PagosFormComponent } from 'src/app/pages/usuarios/pagos-form/pagos-form.component';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { NgxSpinnerModule } from 'ngx-spinner';



@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    HttpClientModule,
    NgbModule,
    ClipboardModule,
    ReactiveFormsModule,
    MatDialogModule,
    ButtonModule,
    NgxMaskModule,
    MatSelectModule,
    MatTableModule,
    NgxSpinnerModule


  ],
  declarations: [
    DashboardComponent,
    PagosComponent,
    UsuariosComponent,
    UsuariosFormComponent,
    PagosFormComponent,
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    
    {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: true}},
  ],
})

export class AdminLayoutModule {}
