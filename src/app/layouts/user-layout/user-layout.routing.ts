import { Routes } from '@angular/router';
import { PagosUsuariosComponent } from 'src/app/pages/pagos-usuarios/pagos-usuarios.component';

export const UserLayoutRoutes: Routes = [
    { path: 'mis-pagos',   component: PagosUsuariosComponent },
];
